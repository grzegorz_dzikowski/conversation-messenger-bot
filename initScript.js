function setEngine(en)
{
	this.engine = en;
}

function setJS(en){
	this.js = en;
}

function sendMessage(value, sleepTimeout, midSleep){
	engine.sendMessage(value, sleepTimeout, midSleep);
}

function sendMessage(value){
	engine.sendMessage(value);
}

function sendFile(filePath, message){
	engine.sendFile(filePath, message);
}

function sendFile(filePath){
	engine.sendFile(filePath, "---");
}

function shutdownEngine(){
	engine.shutdown();
}

function urlApiRequest(url){
	return engine.urlApiRequest(url);
}

function message(message){
}

function reloadScript(){
	js.restartEngine();
}

String.prototype.containsIgnore = function(txt){
	txt = txt.toLowerCase();
	var lower = this.toLowerCase();
	return lower.contains(txt);
};	

String.prototype.equalsIgnore = function(txt){
	txt = txt.toLowerCase();
	var lower = this.toLowerCase();
	return lower == txt;
};

function random(min, max)
{
    return Math.random() * (max - min) + min;
}
function randomFromArray(){
    return arguments[random(0, arguments.length)]
}
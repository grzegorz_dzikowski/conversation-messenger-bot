function message(message) {
	var msg = message.message;
	var user = message.user;
	if(msg.containsIgnore("dobranoc") && msg.containsIgnore("wszystkim")){
		sendMessage("Dobranoc " + user + "! Słodkich, puszystych snów :3");
	}
	if(msg.containsIgnore(":V")){
		sendFile("emoji.png")
	}
	if (msg.containsIgnore("21:37")) {
			sendMessage(rand("*SWOOOOJĄ BAAAARKĘĘĘĘĘ POZOSTAWIAM NA BRZEGUUUUU*", "*OOOOO PANIE, TO TY NA MNIE SPOJRZAAAAŁEŚ*"))
	}
	if (msg.containsIgnore("voli", "bot")) {
		if (msg.containsIgnore("witaj") || msg.containsIgnore("cześć") || msg.containsIgnore("hej") || msg.containsIgnore("czesc")) {
			sendMessage(rand("Cześć @" + user +"! <3 Opowiedz nam, jak się czujesz i co chcesz robić :)", "O witaj! Jak ci mija dzień?", "Hejo!", "Haj :3", "Heloł", "*macha na przywitanie*", "@"+user+"! 👋👋👋👋👋"))
		} else if (msg.containsIgnore("dobranoc")) {
			sendMessage("Dobranoc, @" + user + ". Miło, że pamiętasz o mnie. Pamiętaj - mam wartę 24 godzinną :) ")
		} else if (msg.containsIgnore("dzięki") || msg.containsIgnore("dziękuje")) {
			sendMessage("Cała przyjemność po mojej stronie :)")
		} else if (msg.containsIgnore("przytul")) {
			sendMessage("Chodź do mnie <3 *tuli bardzo mocno w futerko*")
		} else if (msg.containsIgnore("draven")) {
			if (message.user == "Mateusz Grabowski") {
				sendMessage("Draven? DRAVEN? TEN BÓG!!! OMG JAKI ZASZCZYT!!")
			} else {
				sendMessage("Oh, Draven? Wiesz, jakim jestem fanem Dravena??")
			}
		}
		else if(msg.containsIgnore("zdjęcie") || msg.containsIgnore("zdjecie")){
			sendMessage("Ups, przysnąłem. To twoja fotka... Chyba że żadnej nie ma");
		}
		else if(msg.containsIgnore("kocham") && msg.containsIgnore("was")){
			sendMessage("Użytkownik " + user + "! KOCHA WAS WSZYSTKICH. JA TEŻ WAS KOCHAM ❤ ❤ ❤");
		}
	} else if (msg.startsWith("!")) {
		msg = msg.substr(1, msg.length);
		if (msg.equalsIgnore("oke")) {
			sendMessage("Oke");
		} else if (msg.equalsIgnore("draven")) {
			sendMessage("Nie wywołuj Boga z lasu, bo Monster po ciebie przyjdzie :)\n" +
				"All glory to the master lord Draven!")
		} else if (msg.equalsIgnore("przywitanie")) {
			sendMessage("Volimiś zgłasza się do służby!\n Po komendy, wpisz !pomoc\nJestem póki co w fazie beta, ale będę pracował nad sobą :) \nTylko nie nazywać mnie grubym!")
		} else if (msg.equalsIgnore("co")) {
			sendMessage("MESSENGER BOT V3 wersja 0.1Alpha\n" +
				" stworzony dla grupy 'Cosplay, Gierki i Monsterki'.\n" +
				"Jestem miły :)")
		} else if (msg.equalsIgnore("draven?")) {
			sendMessage("DRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVENNNNNNN!!!!!")
		} else if (msg.startsWith("echo")) {
			sendMessage("To jest internet, nie zamknięty pokój, ale masz swoje echo: " + msg.substr(5, msg.length))
		} else if (msg.startsWith("ping")) {
			if(msg.containsIgnore("podkręcona")){
				sendMessage("Aaaaaaa... Kurde. nie dałem rady :( Punkt dla ciebie", "*paruje podkręcenie* Pong", "To jest podkręcona? Ehh Pong", "Leci leci i bum, odbiłem", "Tak podkręcona że nie trafiona w stół. Punkt dla mnie", "O, gdzie ona? Dobrze podkręcona, Punkt dla ciebie ")
			}
			else{
				sendMessage(rand("Pong :)", "Aaaa... pong!", "Pongng", "Ponging", "Ptong", "Ping.. A nie, jednak pong", "Shiet, nie dałem rady. Tą rundę wygrywasz", "Łap podkręconą... Ha, nie da rady, punkt dla mnie", "GAME OVER, punkt dla mnie", "Piłka nie trafiona, punkt dla ciebie"));
			}
		} else if (msg.equalsIgnore("pong")) {
			sendMessage("Emmm, Ping?")
		} else if (msg.equalsIgnore("bóg")) {
			sendMessage("Bóg jest tylko jeden i jego imię rymuje się z 'Draven'")
		} else if (msg.equalsIgnore("dobranoc")) {
			sendMessage("Dobranoc <3 <3")
		} else if (msg.equalsIgnore("lenny")) {
			sendMessage(rand("( ͡° ͜ʖ ͡°)", "(☭ ͜ʖ ☭)", "( ° ͜ʖ °)", "( ͡~ ͜ʖ ͡°)", "( ͡° ͜ʖ ͡ °)"))
		} else if (msg.equalsIgnore("monster")) {
			sendMessage("Tactical monster incoming!!")
			sendFile("monster.png")
		} else if(msg.equalsIgnore("jd")){
			sendMessage("Chodzi o John Deere, dostawcę ciągników?");
		}
		else if(msg.equalsIgnore("nope")){
			sendFile("nope.png", "nope")
		}
		else if(msg.equalsIgnore("source")){
			sendMessage("Jestem na bitbucketcie! \nZnajdziesz mnie tutaj https://bitbucket.org/grzegorz_dzikowski/conversation-messenger-bot/src/master/ \nJuż teraz możesz mieć własnego bota! :)");
		}
		else if(msg.equalsIgnore("[*]")){
			sendMessage("[*]")
			sendFile("candle.jpg");
		}
		else if(msg.equalsIgnore("zabawa")){
			sendMessage("Chcesz mi dopisać komendy? Albo zachowanie?\nMasz tutaj przykład i wyślij potem plik @Grzegorz Dzikowski\nhttps://bitbucket.org/grzegorz_dzikowski/conversation-messenger-bot/src/master/cosplayscript.js");
		}
		else if(msg.equalsIgnore("atencja")){
			sendFile("atencja.png");
		}
		else if(msg.equalsIgnore("Tunak")){
			sendMessage("Tunak Tunak Tun Da da da!!");
		}
		else if(msg.equalsIgnore("ts3")){
			sendMessage("TS3 Servera: lolpyrkon zapto org (zamien spacje na kropki)");
		}
		else {
			sendMessage("Wykryłem jakąś komendę!\n Brzmi ona " + msg + " \nWysłana przez " + user + "\nNiestety, Volimiś nie zna tej komendy :( Ale będzie się starał jej nauczyć")
		}
	}
}

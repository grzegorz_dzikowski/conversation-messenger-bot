package com.dzingishan.messengerbot

import java.io.File
import java.io.FileReader
import java.util.logging.Logger
import javax.script.Invocable
import javax.script.ScriptContext
import javax.script.ScriptEngineManager
import javax.script.SimpleBindings


class JavaScriptEngine(private val messengerEngine: MessengerEngine, private val script: File) {
    val logger = Logger.getLogger("com.dzingishan.messengerbot.JavaScriptEngine")

    val javaScriptEngine = ScriptEngineManager().getEngineByName("nashorn")
    val functionInvoke = javaScriptEngine as Invocable
    var globalBindings = SimpleBindings()

    fun initEngine() {
        logger.info("Starting JavaScriptEngine...")
        javaScriptEngine.setBindings(globalBindings, ScriptContext.ENGINE_SCOPE)
        javaScriptEngine.eval(FileReader("initScript.js"))
        functionInvoke.invokeFunction("setEngine", messengerEngine)
        functionInvoke.invokeFunction("setJS", this)
        loadScript()
    }

    fun restartEngine() {
        globalBindings = SimpleBindings()
        initEngine()
    }

    private fun loadScript() {
        logger.info("Loading script...")
        try {
            javaScriptEngine.eval(FileReader(script))
        } catch (ex: Exception) {
            logger.warning("Error in script!")
            ex.printStackTrace()
            printStackTrace(ex, "Script Error!")
        }
    }

    fun message(message: Message) {
        try {
            functionInvoke.invokeFunction("message", message)
        } catch (ex: Exception) {
            logger.warning("Error in message script!")
            printStackTrace(ex, "Error in executing message!")
        }
    }

    fun stopEngine() {
        //Emm, nothing?
    }
}
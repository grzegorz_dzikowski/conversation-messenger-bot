package com.dzingishan.messengerbot

import org.json.JSONObject
import java.io.File
import java.lang.Thread.sleep
import java.net.URL
import java.time.Instant
import javax.imageio.ImageIO

/**
 * This is sample bot system for my messenger group for cosplaying
 */
fun main(args: Array<String>) {
    println("Starting CosplayPartyBot example")
    val obj = JSONObject(File("config.json").readText())
    val engine = MessengerEngine(username = obj.getString("username"), password = obj.getString("password"), conversationLink = obj.getString("conversation"), cookies = File("cookies.db"), headless = true)
    engine.registerMessageEvent(prefix = "*", eventListener = volibearGenericConversation)
    engine.registerMessageEvent(prefix = "!", eventListener = volibearCommands)
    engine.start()
}

val volibearGenericConversation: (Message, MessengerEngine) -> Unit = { message, engine ->
    if (message.message.contains("voli", true)) {
        if (message.message.contains("witaj", true) || message.message.contains("cześć", true) || message.message.contains("hej", true) || message.message.contains("czesc", true)) {
            engine.sendMessage("Cześć @${message.user}! <3 . Mamy teraz ${currentTime()}. Opowiedz nam, jak się czujesz i co chcesz robić :)")
        } else if (message.message.contains("dobranoc", true)) {
            engine.sendMessage("Dobranoc, @${message.user}. Miło, że pamiętasz o mnie. Pamiętaj - mam wartę 24 godzinną :) ")
        } else if (message.message.contains("dzięki", true) || message.message.contains("dziękuje", true)) {
            engine.sendMessage("Cała przyjemność po mojej stronie :)")
        } else if (message.message.contains("przytul", true)) {
            engine.sendMessage("Chodź do mnie <3 *tuli bardzo mocno w futerko*")
        } else if (message.message.contains("misiu", true)) {
            if (message.user == "Grzegorz Dzikowski") {
                engine.sendMessage("Emm, drugie wcielenie nie powinno się 'misiać'. Ale jak trzeba to możemy :)")
            } else if (message.user == "Martyna Adelajda Grzecha Orczykowska" || message.user == "Angelika Kwaśniewska") {
                engine.sendMessage("Awwwwwwww ^_^ ^_^. Chodź się przytulić <3 <3")
            } else {
                engine.sendMessage("Możemy się przytulić? Proszę :3 ☺")
            }
        } else if (message.message.contains("draven", true)) {
            if (message.user == "Mateusz Grabowski") {
                engine.sendMessage("Draven? DRAVEN? TEN BÓG!!! OMG JAKI ZASZCZYT!!")
            } else {
                engine.sendMessage("Oh, Draven? Wiesz, jakim jestem fanem dravena??")
            }
        }
    }
}

val volibearCommands = { message: Message, engine: MessengerEngine ->
    println("Processing command $message")
    if (message.message.count { it == '!' } == message.message.length) {

    } else {
        if (message.message.equals("draven", true)) {
            engine.sendMessage("Nie wywołuj Boga z lasu, bo Monster po ciebie przyjdzie :)\n" +
                    "All glory to the master lord Draven!")
        } else if (message.message.equals("kotek", false)) {
            engine.sendMessage("Kotki <3 <3 (chwilę czekajcie, ogarniam jakieś z internetu)")
            val urles = engine.urlApiRequest("http://thecatapi.com/api/images/get.php")
            val urle = URL(urles)
            ImageIO.write(ImageIO.read(urle), "JPEG", File("kitten.jpg"))
            engine.sendFile(File("kitten.jpg"))
        } else if (message.message.equals("cofnij", true)) {
            engine.sendMessage("Volimiś idzie spać. :( Mam mało HP i Many, muszę się cofnąć. Jak wrócę, to na pewno napiszę! Dobranoc <3 <3")
            engine.shutdown()
        } else if (message.message.equals("przywitanie", ignoreCase = true)) {
            engine.sendMessage("Volimiś zgłasza się do służby!\n Aktualnie mamy: ${Instant.now()}\nPo komendy, wpisz !pomoc\nJestem póki co w fazie beta, ale będę pracował nad sobą :) \nTylko nie nazywać mnie grubym!")
        } else if (message.message.equals("co", true)) {
            engine.sendMessage("MESSENGER BOT V3 wersja 0.1Alpha\n" +
                    " stworzony dla grupy 'Cosplay, Gierki i Monsterki'.\n" +
                    "Jestem miły :)")
        } else if (message.message.equals("pomoc", ignoreCase = true)) {
            engine.sendMessage("Będę pomocny jak się da. Moje aktualnie działające komendy to:\n" +
                    "!przywitanie - witam się :)\n" +
                    "!co - wyświetla informacje o mnie \n" +
                    "!fotka - emm wysyłam fotkę... rekina?\n" +
                    "!kotek - emmm owwww" +
                    "!meme, !xd - wysyłam randomowe meme z 9gaga\n" +
                    "!draven - boska komenda\n" +
                    "!draven? - Draven?? DRAVEN??\n" +
                    "!echo - !echo\n" +
                    "!ping - Ping\n" +
                    "!pong - Pong\n" +
                    "!anime - wyszukiwarka anime i losowe anime\n" +
                    "!bóg - szukam informacje o bogu w internecie\n" +
                    "!słowa - słowa nie ranią\n" +
                    "!joachim - Pomocy\n" +
                    "" +
                    "I to tak naprawdę wszystko :(\n" +
                    "Cały czas się rozwijam :)\n")
        } else if (message.message.equals("fotka", ignoreCase = true)) {
            engine.sendMessage("Uwaga uwaga - próbuje wysłać zdjęcie. Potrwa to chyba 5 sekund.  Mogę się wykrzaczyć!")
            engine.sendFile(File("random.jpg"))
        } else if (message.message.contains("tuli", true)) {
            engine.sendMessage("Już rzucam Q, żeby być przy tobie szybciej....")
            sleep(1000)
            engine.sendMessage("Tuli tuli <3 *tuli się do ${message.user?.split(" ")?.get(0) ?: "... nie wiem kogo :P"}*")
        } else if (message.message.equals("meme", ignoreCase = true) || message.message.equals("xd", false)) {
            engine.sendMessage("Wysyłam śmieszne meme (chwilę to potrwa, muszę go ściągnąć z 9gaga i wysłać)")
            downloadRandom9gagImage()
            engine.sendFile(File("random_meme.jpg"))
        } else if (message.message.equals("draven?", true)) {
            engine.sendMessage("DRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVENNNNNNN!!!!!")
        } else if (message.message.startsWith("echo", true)) {
            engine.sendMessage("To jest internet, nie zamknięty pokój, ale masz swoje echo: ${message.message.drop(5)}")
        } else if (message.message.equals("ping", true)) {
            engine.sendMessage("Pong :)")
        } else if (message.message.equals("pong", true)) {
            engine.sendMessage("Emmm, Ping?")
        } else if (message.message.equals("anime", true)) {
            engine.sendMessage("Jestem na etapie SAO, obejrzałem One Punch Mana i Shimonetę. Na razie nic nie chce więcej.\nAle nauczę się wszystkich anime na świecie i będę rzucał losowymi. Ok? :)")
        } else if (message.message.equals("bóg", true)) {
            engine.sendMessage("Bóg jest tylko jeden i jego imię rymuje się z 'Draven'")
        } else if (message.message.equals("joachim", true)) {
            engine.sendMessage("NIE POZWÓLCIE MU MNIE DOTKNĄĆ!!! ON I JEGO BROŃ O NAZWIE 'C++' I 'HTML' MOŻE NAS ZNISZCZYĆ!!!")
        } else if (message.message.equals("słowa", true)) {
            engine.sendMessage("Szukanie słów.... Nope jeszcze, nie ma tego XD :) :P")
        } else if (message.message.equals("dobranoc", false)) {
            engine.sendMessage("Dobranoc <3 <3")
        } else if (message.message.equals("lenny", true)) {
            engine.sendMessage("( ͡° ͜ʖ ͡°)")
        } else if (message.message.equals("threshu", true)) {
            if (message.user == "Błażej Ibusz") {
                engine.sendMessage("THRESH WYSYŁA THRESHA")
            } else {
                engine.sendMessage("T H R E S H U")
            }
            engine.sendFile(File("threshu.png"))
        } else if (message.message.equals("monster", true)) {
            engine.sendMessage("Tactical monster incoming!!")
            engine.sendFile(File("monster.png"))
        } else {
            engine.sendMessage("Wykryłem jakąś komendę!\n Brzmi ona '${message.message}' \n Wysłana przez ${message.user}\n Niestety, Volimiś nie zna tej komendy :( Ale będzie się starał jej nauczyć")
        }
    }
}

/*




 */
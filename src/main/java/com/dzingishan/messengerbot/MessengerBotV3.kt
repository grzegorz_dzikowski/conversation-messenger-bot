package com.dzingishan.messengerbot

import org.json.JSONObject
import java.io.File

/**
 * Test code for simple message echo
 */
fun main(args: Array<String>) {
    val obj = JSONObject(File("config.json").readText())
    val engine = MessengerEngine(username = obj.getString("username"), password = obj.getString("password"), conversationLink = obj.getString("conversation"), cookies = File("cookies.db"), headless = false)
    engine.registerMessageEvent(prefix = "*") { it, _ -> println(it.message + " " + it.user) }
    engine.start()

}
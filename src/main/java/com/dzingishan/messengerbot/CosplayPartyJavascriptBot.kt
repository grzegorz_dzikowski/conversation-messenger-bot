package com.dzingishan.messengerbot

import org.json.JSONObject
import java.io.File
import kotlin.concurrent.thread

fun main(args: Array<String>) {
    startMainEngine()
    startSecondEngine()
}

fun startMainEngine() {
    println("Starting CosplayPartyBotJavascript example")
    val obj = JSONObject(File("config.json").readText())
    val engine = MessengerEngine(username = obj.getString("username"), password = obj.getString("password"), conversationLink = obj.getString("conversation"), cookies = File("cookies.db"), headless = true)
    //val engine = MockMessengerEngine()
    val javaScriptEngine = JavaScriptEngine(engine, File("cosplayscript.js"))
    javaScriptEngine.initEngine()
    engine.registerMessageEvent(prefix = "*", eventListener = { it, _ -> javaScriptEngine.message(it) })
    engine.start()
    thread(start = true) {
        while (true) {
            val command = readLine() ?: continue
            if (command.equals("@restart", true)) {
                javaScriptEngine.restartEngine()
                println("Restarted Engine")
            } else if (command.equals("@stop", true)) {
                javaScriptEngine.stopEngine()
                engine.shutdown()
                System.exit(0)
            }
        }
    }
}

fun startSecondEngine() {
    println("Starting CosplayPartyBotJavascript example 2")
    val obj = JSONObject(File("config_df.json").readText())
    val engine = MessengerEngine(username = obj.getString("username"), password = obj.getString("password"), conversationLink = obj.getString("conversation"), cookies = File("cookies.db"), headless = true)
    //val engine = MockMessengerEngine()
    val javaScriptEngine = JavaScriptEngine(engine, File("cosplayscript_df.js"))
    javaScriptEngine.initEngine()
    engine.registerMessageEvent(prefix = "*", eventListener = { it, _ -> javaScriptEngine.message(it) })
    engine.start()
    thread(start = true) {
        while (true) {
            val command = readLine() ?: continue
            if (command.equals("@restart", true)) {
                javaScriptEngine.restartEngine()
                println("Restarted Engine")
            } else if (command.equals("@stop", true)) {
                javaScriptEngine.stopEngine()
                engine.shutdown()
                System.exit(0)
            }
        }
    }
}
package com.dzingishan.messengerbot

import com.gargoylesoftware.htmlunit.BrowserVersion
import com.google.common.collect.EvictingQueue
import io.github.bonigarcia.wdm.WebDriverManager
import org.openqa.selenium.*
import org.openqa.selenium.chrome.ChromeDriver
import org.openqa.selenium.chrome.ChromeOptions
import org.openqa.selenium.htmlunit.HtmlUnitDriver
import org.openqa.selenium.support.ui.FluentWait
import java.io.*
import java.lang.Thread.sleep
import java.util.concurrent.TimeUnit
import java.util.logging.Logger
import kotlin.concurrent.thread


val httpUnitDriver = HtmlUnitDriver(BrowserVersion.FIREFOX_52, false)


/**
 * Main Messenger Engine - connects to the facebook, reads messages and send files/messages
 * @param username - Username used to login, usually username
 * @param password - Password used to login
 * @param conversationLink - Conversation to monitor. To get it, go to m.facebook.com on Google Chrome, login, go to the desired conversationLink and copy the ling from the address bar
 * @param errorRepeater - max times system can repeat during error
 * @param headless - if true, google chrome will start in headless mode. otherwise, the windows will start. Good for debugging
 * @param cookies - supply cookies file. If it's null, it will login and don't
 * @param autoRefresh - every how much seconds, browser refresh should be performed
 * @param autoRestart - every how much minutes, browser should be restarted
 */
open class MessengerEngine(private val username: String, private val password: String, private val conversationLink: String, private val headless: Boolean = true, private val errorRepeater: Int = 10, private val cookies: File? = null, private val autoRefresh: Int = 300, private val autoRestart: Int = 60) {


    val logger = Logger.getLogger("com.dzingishan.messengerbot.MessengerEngine")

    var eventsListeners = mapOf<String, (Message, MessengerEngine) -> Unit>()

    /**
     * This is supporting driver, for other requests and downloading some data
     */
    private val supportDriver = HtmlUnitDriver(BrowserVersion.FIREFOX_52)

    /**
     * Main Driver for communicating with facebook
     */
    private lateinit var mainDriver: WebDriver
    /**
     * Supporting object for javascript
     */
    private lateinit var javaScript: JavascriptExecutor

    /**
     * Thread, on which engine works
     */
    private lateinit var readingThread: Thread

    /**
     * Queue, that holds 10 last processed messages
     */
    private val processedMessages = EvictingQueue.create<Message>(10)

    /**
     * Initializes the engine - creates needed drivers and functions
     */
    private fun initializeEngine() {
        WebDriverManager.chromedriver().setup()
        logger.fine("Creating Chrome Driver")
        mainDriver = createChromeDriver(headless)
        javaScript = mainDriver as JavascriptExecutor
    }

    /**
     * Registers new message listener
     * Note that can be only one listener for one prefix
     * * is listener for any message
     */
    fun registerMessageEvent(prefix: String = "!", eventListener: (message: Message, engine: MessengerEngine) -> Unit) {
        eventsListeners += prefix to eventListener
    }

    private var errorCounter = 0
    private var lastRefresh = System.currentTimeMillis()
    private var lastRestart = System.currentTimeMillis()

    private var lastMessage = ""

    /**
     * Starts messenger bot engine and listener. Creates new Thread
     * TODO: Check if start was executed
     */
    open fun start() {
        logger.info("Starting Messenger Engine")
        //Prevents double starting
        if (::readingThread.isInitialized) {
            throw IllegalAccessException("Already initialized!")
        }

        readingThread = thread(start = true) {
            //Final exception for crash
            var finalException: Exception
            //Main error loop
            while (errorCounter++ < errorRepeater) {
                logger.info("Initializing system")
                initializeEngine()
                initialize()
                try {
                    logger.info("Starting reading thread")
                    //Infinity loop of the reading system
                    while (true) {
                        //Checks if its important to refresh the page
                        if (lastRefresh + (autoRefresh * 1000) <= System.currentTimeMillis()) {
                            lastRefresh = System.currentTimeMillis()
                            refreshPage()
                        }
                        if (lastRestart + (autoRestart * 60000) <= System.currentTimeMillis()) {
                            lastRestart = System.currentTimeMillis()
                            restartBrowser()
                        }
                        val sevenMessages = (getLastSevenMessages())
                        sevenMessages.forEach { message ->
                            logger.fine("Read $message")
                            if (message.messageState == MessageState.UNKNOWN || processedMessages.contains(message))//Well, something is not yes with message
                                return@forEach
                            processedMessages.add(message)
                            eventsListeners["*"]?.invoke(message, this)
                            eventsListeners.forEach { if (message.message.startsWith(it.key)) it.value.invoke(message.copy(message = message.message.replaceFirst(it.key, "")), this) }
                        }
                    }
                } catch (ex: Exception) {
                    finalException = ex
                    //TODO - better exception handling
                    ex.printStackTrace()
                    logger.info(mainDriver.pageSource)
                    val f = File(File("logs").also { it.mkdirs() }, "crashreport ${System.currentTimeMillis()}")
                    PrintWriter(f).also { it.println(mainDriver.pageSource); it.println(); ex.printStackTrace(it); }.close()
                    mainDriver.quit()
                    //TODO
                }

            }

            logger.info("Shutdown")
            //Do something on final error
        }
    }

    /**
     * Simple api request, go to address and returns redirected address (i know, it can be done better)
     */
    open fun urlApiRequest(url: String): String {
        logger.info("Loading $url")
        supportDriver.get(url)
        return supportDriver.currentUrl
    }

    /**
     * Sends message to system
     */
    open fun sendMessage(message: String, sleep: Long = 1000, midTermSleep: Long = 0) {
        logger.fine("Sending $message")
        try {
            mainDriver.findElement(By.id("composerInput")).sendKeys(message)
            Thread.sleep(midTermSleep)
            mainDriver.findElement(By.name("send")).click()
            Thread.sleep(sleep)
        } catch (ex: Exception) {
            logger.severe("Error with ${ex.message}")
            if (ex.message!!.contains("BMP")) {
                logger.info("Emoji detected. Deleting message...")
                sendMessage("Bleee, nie chce tych emotek. Ić sb :(") //TODO: Localization?
            } else {
                ex.printStackTrace()
            }
        }
    }

    open fun sendMessage(message: String) {
        sendMessage(message, 1000, 0)
    }

    open fun sendFile(file: String, message: String = " ") {
        sendFile(File(file), message)
    }

    /**
     * Sends given file. NOTE: Should be only image. Not too large. Max 30 seconds of sending
     */
    open fun sendFile(file: File, message: String = " ") {
        logger.info("Sending file ${file.absolutePath}")
        mainDriver.findElement(By.name("photo")).sendKeys(file.absolutePath)
        try {
            FluentWait<WebDriver>(mainDriver).withTimeout(30, TimeUnit.SECONDS).pollingEvery(200, TimeUnit.MILLISECONDS).ignoring(NoSuchElementException::class.java)
        } catch (ex: Exception) {
            ex.printStackTrace()
            logger.warning("Sending not possible")
            mainDriver.navigate().refresh()
        }
        sendMessage(message, midTermSleep = 500)
        sleep(1000)
        mainDriver.findElement(By.name("send")).click()
        sleep(500)
        logger.info("Sended image!")
    }

    /**
     * Opens facebook and tries to open the desired site
     */
    private fun initialize() {
        logger.info("Logging into the system...")
        mainDriver.get("https://m.facebook.com/")
        if (cookies != null) {
            //Loading cookies and trying to login
            if (!loadCookies(cookies) || !checkLogin()) {
                logger.info("Not logged in. logging in!")
                login()
                saveCookies(cookies)
            }
        } else {
            //Well, no cookies = login. Not recommended tho
            logger.info("Logging in without cookies!")
            login()
        }
        mainDriver.get(conversationLink)
    }

    /**
     * Refreshes the page in browser
     */
    private fun refreshPage() {
        logger.info("Refreshing site")
        mainDriver.navigate().refresh()
    }

    private fun restartBrowser() {
        logger.info("Restarting the browser")
        mainDriver.quit()
        initializeEngine()
        initialize()
    }

    /**
     * Performs login on login site
     */
    private fun login() {
        logger.info("Logging in...")
        mainDriver.findElement(By.name("username")).sendKeys(username)
        mainDriver.findElement(By.name("pass")).sendKeys(password)
        mainDriver.findElement(By.name("login")).click()
        Thread.sleep(2000)
    }

    /**
     * Saves cookies to the file
     * @param cookies - Cookies file to save to
     */
    private fun saveCookies(cookies: File) {
        logger.fine("Saving Cookies")
        ObjectOutputStream(FileOutputStream(cookies)).also { it.writeObject((mainDriver.manage().cookies)) }.close()
    }

    /**
     * Load cookies from the file
     * @param cookies - file to read from
     * @return True if operation successful, otherwise false
     */
    private fun loadCookies(cookies: File): Boolean {
        if (!cookies.exists())
            return false
        logger.fine("Loading cookies")
        ObjectInputStream(FileInputStream(cookies)).also { (it.readObject() as Set<Cookie>).forEach(mainDriver.manage()::addCookie) }.close()
        mainDriver.get("https://m.facebook.com/")
        return true
    }


    /**
     * Checks if system is logged in
     */
    private fun checkLogin(): Boolean {
        return (mainDriver.findElements(By.id("m_login_email")).isEmpty())
    }


    /**
     * Retrieves last 7 messages
     */
    private fun getLastSevenMessages(): List<Message> {
        return mainDriver.findElements(By.className("c")).reversed().subList(0, 7).map { parent ->
            try {
                val ele3 = parent.findElements(By.className("msg")).reversed()[0]
                val url = ele3.findElement(By.className("actor-link")).getAttribute("href")
                val msg = ele3.text.split("\n").reversed()[0]
                val who = ele3.text.split("\n")[0]
                var img: String? = null
                if (!parent.findElements(By.className("messageAttachments")).isEmpty()) {
                    val attachments = parent.findElement(By.className("messageAttachments"))
                    if (!attachments.findElements(By.tagName("img")).isEmpty()) {
                        img = attachments.findElement(By.tagName("img")).getAttribute("src")
                    }

                }
                Message(message = msg, url = url, user = who, attachment = img)

            } catch (ex: Exception) {
                Message(message = "invalid", user = "invalid", url = "invalid", messageState = MessageState.UNKNOWN)
            }
        }


    }

    /**
     * Reads last message from messenger chat
     */
    private fun readLastMessage(): Message {
        return try {
            val parent = mainDriver.findElements(By.className("c")).reversed()[0]
            val ele3 = parent.findElements(By.className("msg")).reversed()[0]
            val url = ele3.findElement(By.className("actor-link")).getAttribute("href")
            val msg = ele3.text.split("\n").reversed()[0]
            val who = ele3.text.split("\n")[0]
            if (!parent.findElements(By.className("messageAttachments")).isEmpty()) {
                val attachments = parent.findElement(By.className("messageAttachments"))
                if (!attachments.findElements(By.tagName("img")).isEmpty()) {
                    val imgSrc = attachments.findElement(By.tagName("img")).getAttribute("src")
                    Message(message = msg, url = url, user = who, attachment = imgSrc)
                }
            }
            Message(message = msg, url = url, user = who)
        } catch (ex: Exception) {
            Message(message = "invalid", user = "invalid", url = "invalid", messageState = MessageState.UNKNOWN)
        }
    }

    /**
     * Closes the engine
     */
    fun shutdown() {
        readingThread.interrupt()
        mainDriver.close()
        supportDriver.close()
    }

}


/**
 * Data class used to read messages
 * @user - the name of the person who send the message
 * @url - the url to the profile of the person
 * @message - message sent by this person
 */
data class Message(val user: String?, val url: String?, var message: String, var messageState: MessageState = MessageState.MESSAGE, var attachment: String? = null) {
    override operator fun equals(other: Any?): Boolean {
        if (other !is Message)
            return false
        return (other.user == user && message == message && attachment == attachment)
    }
}

/**
 * Enum class for future message type detection
 * Now used to filter invalid messages
 */
enum class MessageState {
    MESSAGE, UNKNOWN
}

/**
 * Creates and initializes ChromeDriver and the browser
 * @param headless - start in headless mode if true
 */
private fun createChromeDriver(headless: Boolean = true): ChromeDriver {
    val options = ChromeOptions()
    if (headless) {
        options.addArguments("--headless")
        options.addArguments("--no-sandbox")
        options.addArguments("--disable-dev-shm-usage")
        options.addArguments("--window-size=1400,1400")
    }
    return ChromeDriver(options)
}
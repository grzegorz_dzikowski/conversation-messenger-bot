@file:JvmName("Utils")

package com.dzingishan.messengerbot

import java.io.File
import java.io.PrintWriter
import java.net.HttpURLConnection
import java.net.URL
import java.time.Instant
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*
import javax.imageio.ImageIO

/**
 * Gets current time in Locale.UK
 */
fun currentTime(): String {
    val formatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
            .withLocale(Locale.UK)
            .withZone(ZoneId.systemDefault())
    return formatter.format(Instant.now())

}


/**
 * Well, download random image from 9gag, can be part of video or post image
 */
fun downloadRandom9gagImage() {
    val url = URL("https://9gag.com/random")
    val con = url.openConnection() as HttpURLConnection
    con.instanceFollowRedirects = false
    con.requestMethod = "GET"
    con.responseCode
    val imgAddress = (con.getHeaderField("location").split("/").reversed()[0])
    ImageIO.write(ImageIO.read(URL("https://img-9gag-fun.9cache.com/photo/${imgAddress}_700b.jpg")), "JPEG", File("random_meme.jpg"))
}

/**
 * Writes stacktrace to file
 */
fun printStackTrace(ex: Exception, message: String) {
    ex.printStackTrace()
    println(message)
    val f = File("crashreport${System.currentTimeMillis()}")
    PrintWriter(f).also { it.println(message); it.println(); ex.printStackTrace(it); }.close()
}
//Add random Chuck Norris jokes http://api.icndb.com/jokes/random
package com.dzingishan.messengerbot.testing

import com.dzingishan.messengerbot.Message
import com.dzingishan.messengerbot.MessengerEngine
import kotlin.concurrent.thread

class MockMessengerEngine : MessengerEngine("", "", "") {
    override fun sendMessage(message: String, sleep: Long, midTermSleep: Long) {
        println("Message: " + message)
    }

    override fun sendMessage(message: String) {
        println("Message notimer: " + message)

    }


    override fun start() {
        println("Starting mock engine!")
        thread(start = true) {
            var person = "console tester"
            while (true) {
                val command = readLine() ?: continue
                println("command: $command")
                if (command.startsWith("@person")) {
                    val remain = command.removePrefix("@person ")
                    person = remain
                    continue
                }
                val message = Message(url = "", user = person, message = command)
                eventsListeners["*"]?.invoke(message, this)
                eventsListeners.forEach { if (message.message.startsWith(it.key)) it.value.invoke(message.copy(message = message.message.replaceFirst(it.key, "")), this) }
            }
        }
    }

    override fun sendFile(file: String, message: String) {
        println("SENDING FILE $file with message $message")
    }

    override fun urlApiRequest(url: String): String {
        println("Requesting $url")
        return "REQUEST FAILED"
    }
}
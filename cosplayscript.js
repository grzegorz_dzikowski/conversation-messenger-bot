function message(message) {
	var msg = message.message;
	var user = message.user;
	if (msg.containsIgnore("voli")) {
		if (msg.containsIgnore("witaj") || msg.containsIgnore("cześć") || msg.containsIgnore("hej") || msg.containsIgnore("czesc")) {
			sendMessage("Cześć @" + user +"! <3 Opowiedz nam, jak się czujesz i co chcesz robić :)")
		} else if (msg.containsIgnore("dobranoc")) {
			sendMessage("Dobranoc, @" + user + ". Miło, że pamiętasz o mnie. Pamiętaj - mam wartę 24 godzinną :) ")
		} else if (msg.containsIgnore("dzięki") || msg.containsIgnore("dziękuje")) {
			sendMessage("Cała przyjemność po mojej stronie :)")
		} else if (msg.containsIgnore("przytul")) {
			sendMessage("Chodź do mnie <3 *tuli bardzo mocno w futerko*")
		} else if (msg.containsIgnore("misiu")) {
			if (message.user == "Grzegorz Dzikowski") {
				sendMessage("Emm, drugie wcielenie nie powinno się 'misiać'. Ale jak trzeba to możemy :)")
			} else if (message.user == "Martyna Adelajda Grzecha Orczykowska" || message.user == "Angelika Kwaśniewska") {
				sendMessage("Awwwwwwww ^_^ ^_^. Chodź się przytulić <3 <3")
			} else {
				sendMessage("Możemy się przytulić? Proszę :3 ☺")
			}
		} else if (msg.containsIgnore("draven")) {
			if (message.user == "Mateusz Grabowski") {
				sendMessage("Draven? DRAVEN? TEN BÓG!!! OMG JAKI ZASZCZYT!!")
			} else {
				sendMessage("Oh, Draven? Wiesz, jakim jestem fanem dravena??")
			}
		}
	} else if (msg.startsWith("!")) {
		msg = msg.substr(1, msg.length);
		if (msg.equalsIgnore("oke")) {
			sendMessage("Oke");
		} else if (msg.equalsIgnore("draven")) {
			sendMessage("Nie wywołuj Boga z lasu, bo Monster po ciebie przyjdzie :)\n" +
				"All glory to the master lord Draven!")
		} else if (msg.equalsIgnore("cofnij")) {
			sendMessage("Volimiś idzie spać. :( Mam mało HP i Many, muszę się cofnąć. Jak wrócę, to na pewno napiszę! Dobranoc <3 <3")
			shutdownEngine()
		} else if (msg.equalsIgnore("przywitanie")) {
			sendMessage("Volimiś zgłasza się do służby!\n Po komendy, wpisz !pomoc\nJestem póki co w fazie beta, ale będę pracował nad sobą :) \nTylko nie nazywać mnie grubym!")
		} else if (msg.equalsIgnore("co")) {
			sendMessage("MESSENGER BOT V3 wersja 0.1Alpha\n" +
				" stworzony dla grupy 'Cosplay, Gierki i Monsterki'.\n" +
				"Jestem miły :)")
		} else if (msg.equalsIgnore("fotka")) {
			sendMessage("Uwaga uwaga - próbuje wysłać zdjęcie. Potrwa to chyba 5 sekund.  Mogę się wykrzaczyć!")
			sendFile("random.jpg")
		} else if (msg.containsIgnore("tuli")) {
			sendMessage("Już rzucam Q, żeby być przy tobie szybciej....")
			setTimeout(function () {
				sendMessage("Tuli tuli <3 *tuli się do " + user + "*")
			}, 2000);
		} else if (msg.equalsIgnore("meme") || msg.equalsIgnore("xd")) {
			sendMessage("Wysyłam śmieszne meme (chwilę to potrwa, muszę go ściągnąć z 9gaga i wysłać)")
			var Utils = Java.type("com.dzingishan.messengerbot.Utils");
			Utils.downloadRandom9gagImage()
			sendFile("random_meme.jpg")
		} else if (msg.equalsIgnore("draven?")) {
			sendMessage("DRAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAVENNNNNNN!!!!!")
		} else if (msg.startsWith("echo")) {
			sendMessage("To jest internet, nie zamknięty pokój, ale masz swoje echo: " + msg.substr(5, msg.length))
		} else if (msg.equalsIgnore("ping")) {
			sendMessage("Pong :)")
		} else if (msg.equalsIgnore("pong")) {
			sendMessage("Emmm, Ping?")
		} else if (msg.equalsIgnore("anime")) {
			sendMessage("Jestem na etapie SAO, obejrzałem One Punch Mana i Shimonetę. Na razie nic nie chce więcej.\nAle nauczę się wszystkich anime na świecie i będę rzucał losowymi. Ok? :)")
		} else if (msg.equalsIgnore("bóg")) {
			sendMessage("Bóg jest tylko jeden i jego imię rymuje się z 'Draven'")
		} else if (msg.equalsIgnore("joachim")) {
			if (user == "Grzegorz Dzikowski") {
				sendMessage("NIE POZWÓLCIE MU MNIE DOTKNĄĆ!!! ON I JEGO BROŃ O NAZWIE 'C++' I 'HTML' MOŻE NAS ZNISZCZYĆ!!!")
			} else if (user == "Joachim Jarosz") {
				sendMessage("Przepraszam, że tak cię nazywałem :(\nTak naprawdę nie jesteś taki zły :)\nPrzytulas na zgodę? :3");
			} else {
				sendMessage("Ten miły człowiek? Znam go chyba...");
			}
		} else if (msg.equalsIgnore("słowa")) {
			sendMessage("Szukanie słów.... Nope jeszcze, nie ma tego XD :) :P")
		} else if (msg.equalsIgnore("dobranoc")) {
			sendMessage("Dobranoc <3 <3")
		} else if (msg.equalsIgnore("lenny")) {
			sendMessage("( ͡° ͜ʖ ͡°)")
		} else if (msg.equalsIgnore("threshu")) {
			if (message.user == "Błażej Ibusz") {
				sendMessage("THRESH WYSYŁA THRESHA")
			} else {
				sendMessage("T H R E S H U")
			}
			sendFile("threshu.png")		
		} else if (msg.equalsIgnore("monster")) {
			sendMessage("Tactical monster incoming!!")
			sendFile("monster.png")
		} else if(msg.equalsIgnore("nope")){
			sendFile("nope.png", "nope")
		}
		else {
			sendMessage("Wykryłem jakąś komendę!\n Brzmi ona " + msg + " \nWysłana przez " + user + "\nNiestety, Volimiś nie zna tej komendy :( Ale będzie się starał jej nauczyć")
		}
	}
}
